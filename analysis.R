# Finometer calibration tests
# 19/04/2023
# by Alex Drysdale
library(DBI)
library(ggplot2)

# Connects to the database
con <- dbConnect(RSQLite::SQLite(), dbname="datalog.sqlite3")
dbListTables(con)

### Evaluates test data set ###

query <- "SELECT * FROM t20230425145516 WHERE Elapsed_Time < 200000 AND Time_Stamp > 230000000000"
d <- dbGetQuery(con, query)
summary(d)
head(d)

# Adjustments
d$Arm_Adjusted <- d$Arm_Pressure + d$Height / 10

actual = data.frame(sys=134, dia=82)

ggplot(data=d, aes(x=Elapsed_Time)) +
  geom_line(aes(y=Finger_Pressure, col='Finger Pressure')) + 
  geom_line(aes(y=Arm_Adjusted, col='Arm Pressure\n(Height Adjusted)')) +
  geom_line(aes(y=Finger_Plethysmogram, col='Finger Pleth')) +
  geom_hline(data=actual, lty=2, aes(yintercept=sys, col="Actual Systolic")) +
  geom_hline(data=actual, lty=2, aes(yintercept=dia, col="Actual Diastolic")) +
  labs(x='Time (ms)', y='Pressure (mmHg)', col='Sensor')


dbDisconnect(con)